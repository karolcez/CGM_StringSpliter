/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringtwocolumns;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Dom
 */
class Spliter {

    void SplitParts(String[] parts, List<String> part1, List<String> part2) {
        int max_size = 18;
        int i = 0;
        int index = 0;
        int index2 = 0;
        int line_size = 0;
        int line_size2 = 0;
        part1.add("");
        part2.add("");

        for (String word : parts) {
            if (i < parts.length / 2) {
                part1.set(index, part1.get(index) + " " + word);
                line_size += word.length();
                if (line_size > max_size) {
                    part1.add("");
                    index++;
                    line_size = 0;
                }
            } else {
                part2.set(index2, part2.get(index2) + " " + word);
                line_size2 += word.length();
                if (line_size2 > max_size) {
                    part2.add("");
                    index2++;
                    line_size2 = 0;
                }
            }
            i++;
        }
    }
}

public class StringTwoColumns {

    public static void main(String[] args) {

        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur hendrerit ultrices lacus in porta. Curabitur ornare bibendum elit, at vulputate nulla laoreet at. Praesent tellus lorem, euismod non auctor ut, volutpat eu ante. Nunc vitae arcu vitae nunc semper venenatis id in lacus. Morbi ac vulputate nunc, sit amet ornare nunc. Donec ac urna dolor. Proin pharetra massa id metus commodo, sed aliquam libero posuere. Aliquam erat volutpat. Aenean a risus vehicula arcu interdum dictum sit amet nec leo. Vestibulum quis diam vel turpis consequat ullamcorper.\n"
                + "Sed imperdiet tincidunt tellus, sed posuere odio pharetra in. Nunc lacinia lectus eu orci ultricies, id viverra leo vehicula. Ut placerat gravida tortor eget sollicitudin. Aliquam sit amet nisl non diam porta bibendum. Curabitur turpis nisl, eleifend auctor libero sit amet, pretium hendrerit augue. Nam sed nibh varius, cursus arcu nec, cursus diam. Pellentesque finibus venenatis erat at pharetra. Cras lacinia nunc vitae ante rhoncus, quis dignissim ante auctor. Praesent aliquet ligula eu quam aliquet euismod. Etiam porttitor facilisis turpis et mattis. Suspendisse quis erat sit amet sapien lacinia feugiat ut sed metus. Aliquam ornare augue ut aliquam suscipit. In sodales risus vel risus luctus placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\n"
                + "Etiam porttitor eros nec neque convallis rhoncus. Aenean imperdiet ligula lorem, ut hendrerit urna sodales ut. Duis mattis odio quis turpis elementum ultricies. Vivamus elementum mi a ligula faucibus, vel accumsan sapien laoreet. Donec in enim lectus. Integer vitae nunc dignissim est maximus pretium. Phasellus ligula sapien, malesuada a consequat vel, vulputate blandit tellus. Vestibulum et sapien nisl. Suspendisse nisi massa, molestie sit amet nunc in, consequat iaculis lorem. Aliquam ornare lorem massa, quis volutpat ex scelerisque quis. In vel porttitor nunc. Morbi metus turpis, vestibulum vitae porttitor et, ullamcorper id nibh. Pellentesque enim sem, tincidunt eget dolor non, consequat accumsan enim. Etiam quis dignissim massa.\n"
                + "Donec at neque ex. Proin eleifend dapibus elit, at viverra magna ornare in. Etiam neque leo, lacinia non risus nec, ultrices porttitor lacus. Fusce justo orci, fringilla nec metus nec, feugiat auctor leo. Sed faucibus ac libero vel ultricies. Vestibulum et ultricies lorem, eu mattis lectus. Morbi turpis diam, tempus quis lectus vel, ornare egestas nisi. Sed ut ipsum eget ex porta pretium ut vitae libero.\n"
                + "Aenean ullamcorper molestie dolor, eu placerat ex sollicitudin nec. Nullam varius dolor et iaculis aliquet. Praesent pellentesque nunc ac ex accumsan ullamcorper. Integer hendrerit volutpat sagittis. Praesent ut massa et mi faucibus faucibus id eu nisi. Morbi laoreet dictum neque eu pretium. Nunc dolor dui, tristique vitae erat semper, varius tempus ligula. Maecenas ut tortor in arcu tincidunt mattis quis nec eros. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras dui quam, lacinia blandit consequat eget, facilisis in massa. Morbi risus est, bibendum sit amet interdum et, finibus at odio. Quisque dolor nisl, venenatis in blandit sit amet, eleifend gravida odio. Integer gravida velit ut consectetur ullamcorper.";
        
        String[] parts = text.split("\\s+");
        
        List<String> part1 = new LinkedList<>();
        List<String> part2 = new LinkedList<>();

        Spliter s = new Spliter();
        s.SplitParts(parts, part1, part2);
        int index = 0;
        while (true) {
            if (index < part1.size() && index < part2.size()) {
                System.out.printf("%-30.60s         %-30.60s%n", part1.get(index), part2.get(index));
            } else if (index >= part1.size() && index >= part2.size()) {
                break;
            } else if (index >= part1.size()) {
                System.out.printf("%-30.60s         %-30.60s%n", " ", part2.get(index));
            } else if (index >= part2.size()) {
                System.out.printf("%-30.60s         %-30.60s%n", part1.get(index), " ");
            }
            index++;
        }

    }

}
